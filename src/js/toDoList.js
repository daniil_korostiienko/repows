'use strict';

angular
    .module('toDoList', [])
    .service('toDoListService', function ($http) {
        this.getData = function () {
            return $http.get('/toDoList').then(function successCallback(response) {
                return response.data;
            }, function errorCallback(error) {
                console.log(error);
            });
        };

        this.addToDo = function (entry) {
            return $http.post('/toDoList', entry).then(function successCallback(response) {
                return response.data;
            }, function errorCallback(error) {
                console.log(error);
            });
        };

        this.deleteDoc = function (entry) {
            return $http.delete('/toDoList/' + entry).then(function successCallback(response) {
                return response.data;
            }, function errorCallback(error) {
                console.log(error);
            });
        };

        this.editDoc = function (entry) {
            return $http.put('/toDoList/' + entry._id, entry).then(function successCallback(response) {
                return response.data;
            }, function errorCallback(error) {
                console.log(error);
            });
        };
    })
    .controller('toDoListController', function ($scope, toDoListService) {
        toDoListService.getData().then(function(response) {
            $scope.data = response;
        });

        var x = {
            name: Math.floor(Math.random() * 100),
            list: [Math.floor(Math.random() * 100), Math.floor(Math.random() * 100), Math.floor(Math.random() * 100)]
        };

        $scope.postSmth = function () {
            toDoListService.addToDo(x).then(function(response) {
                $scope.data.push(response);
            });
        };

        $scope.deleteDoc = function (id) {
            toDoListService.deleteDoc(id).then(function(response) {
                $scope.data = _.filter($scope.data, function(el){
                    return el._id !== response;
                });
            });
        };

        $scope.editDoc = function (entry) {
            entry.name = Math.floor(Math.random() * 10000);

            toDoListService.editDoc(entry).then(function(response) {
                console.log(response);
            });
        };



    });