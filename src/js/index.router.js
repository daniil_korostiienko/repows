toDoApp.config(function($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise("/list");

    $stateProvider
        .state('state1', {
            url: "/list",
            templateUrl: "../html/toDoList.html",
            controller: 'toDoListController'
        });
//        .state('state1.list', {
//            url: "/list",
//            templateUrl: "partials/state1.list.html",
//            controller: function($scope) {
//                $scope.items = ["A", "List", "Of", "Items"];
//            }
//        });
});