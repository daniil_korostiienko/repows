'use strict';

var express = require('express'),
    mongoose = require('mongoose'),
    app = express(),
    bodyParser = require('body-parser'),
    toDoSchema = new mongoose.Schema(
        {
            name: { type: String, default: 'default header' },
            list: Array
        }
    ),
    ToDo = mongoose.model('ToDo', toDoSchema);

mongoose.connect('mongodb://192.168.0.141/test');

app.use('/html', express.static(__dirname + '/src/html'));
app.use('/js', express.static(__dirname + '/src/js'));
app.use('/css', express.static(__dirname + '/src/css'));
app.use('/lib', express.static(__dirname + '/bower_components'));

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.get('/', function (request, response) {
    response.sendFile('/index.html', { root: __dirname });
});

function getList (req, res) {
    ToDo.find({}, function (err, docs) {
        if (!err) { res.json(docs); }
    });
}

function createToDo (req, res) {
    var toDo = new ToDo(req.body);

    toDo.save(function (err) {
        if (err) {
            console.log(err);
        } else {
            console.log('entry saved');
        }
    });

    res.json(req.body);
}

function editToDo (req, res) {
    ToDo
        .update({ _id: req.params.id }, req.body, function (err, doc) {
            if (!err) { res.json(doc); }
        });
}

function deleteToDo (req, res) {
    ToDo
        .remove({ _id: req.params.id }, function (err) {
            if (!err) { res.send(req.params.id); }
        }, true);
}

app.route('/toDoList')
    .get(getList)
    .post(createToDo);

app.route('/toDoList/:id')
    .put(editToDo)
    .delete(deleteToDo);

app.get('*', function (request, response) {
    function isRest () {
        var notRest = ['OData', '.css', '.js', '.map', '.eot', '.ttf', '.svg', '.woff'],
            rest = true;

        notRest.forEach(function (key) {
            if (request.url.indexOf(key) !== -1) {
                rest = false;
            }
        });

        return rest;
    }

    if (isRest()) {
        response.sendFile('/index.html', { root: __dirname });
    }
});

app.listen(3000);

console.log("\nServer started on 127.0.0.1:3000\n");
